package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class DisplayMessageActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE2";
    private String CollarAddress;
    private String ServerAddress;
    private String DogBreed;
    private String DogGender;
    private String DogAge;
    private int DogAge_int;

    private class GetDataFromPost extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... args) {
            // parameters comes from the execute() call:
            // params[0] is the url
            // params[1] is the commend type
            try {
                String return_text = SendPostRequest(args[0], args[1]);
                if (return_text != "") {
                    return return_text;
                } else {
                    return "Nothing received";
                }

            } catch (Exception e) {
                return "Unable to retrieve web page. Url may be invalid: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
//            print_area.setText(result);
            Log.d("CONN",result);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(message);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        DogAge = preferences.getString("DogAge", "");
        EditText dog_info_age = (EditText)findViewById(R.id.dog_info_age);
        dog_info_age.setText(DogAge);

        DogBreed = preferences.getString("DogBreed", "");
        EditText dog_info_breed = (EditText)findViewById(R.id.dog_info_breed);
        dog_info_breed.setText(DogBreed);

        ServerAddress = preferences.getString("ServerAddress", "");
        EditText server_address = (EditText)findViewById(R.id.server_address);
        server_address.setText(ServerAddress);

        CollarAddress = preferences.getString("CollarAddress", "");
        EditText collar_address = (EditText)findViewById(R.id.collar_address);
        collar_address.setText(CollarAddress);

        DogGender = preferences.getString("DogGender", "");
        RadioButton dog_gender_male = (RadioButton)findViewById(R.id.dog_info_gender_male);
        RadioButton dog_gender_female = (RadioButton)findViewById(R.id.dog_info_gender_female);
        if (DogGender.equals("Male")){
            dog_gender_male.setChecked(true);
            dog_gender_female.setChecked(false);
        } else {
            dog_gender_female.setChecked(true);
            dog_gender_male.setChecked(false);
        }

        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_display_message);
        layout.addView(textView);
    }

    public void settingGender(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.dog_info_gender_male:
                if (checked)
                    DogGender = "Male";
                break;
            case R.id.dog_info_gender_female:
                if (checked)
                    DogGender = "Female";
                break;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        Log.d("add", DogGender);
        editor.putString("DogGender",DogGender);
        editor.apply();
    }

    public void settingCollarAddress(View view) {
        EditText collar_address = (EditText) findViewById(R.id.collar_address);
        if(collar_address.isEnabled()){
            CollarAddress = collar_address.getText().toString();
            collar_address.setEnabled(false);
        } else {
            collar_address.setEnabled(true);
        }

        Log.d("C-Addr", CollarAddress);
    }

    public void settingServerAddress(View view) {
        EditText server_address = (EditText) findViewById(R.id.server_address);
        if(server_address.isEnabled()){
            ServerAddress = server_address.getText().toString();
            server_address.setEnabled(false);
        } else {
            server_address.setEnabled(true);
        }

        Log.d("S-Addr", ServerAddress);
    }

    public void settingDogInfo(View view) {
        //settingCollarAddress(view);
        //settingServerAddress(view);

        DogBreed = ((EditText) findViewById(R.id.dog_info_breed)).getText().toString();
        DogAge = ((EditText) findViewById(R.id.dog_info_age)).getText().toString();
        DogAge_int = Integer.valueOf(DogAge);

        // checkAddressValidation()
        Log.d("add", DogAge);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("DogAge",DogAge);
        editor.putString("DogBreed",DogBreed);
        editor.putString("DogGender",DogGender);
        editor.putString("ServerAddress",ServerAddress);
        editor.putString("CollarAddress",CollarAddress);
        editor.apply();

        new GetDataFromPost().execute(CollarAddress, "set_dog_info");
//        new GetDataFromPost().execute(ServerAddress, "test connect");
        finish();


    }
    @Nullable
    private String SendPostRequest(String myurl, String command) throws IOException {

        // Only display the first 500 characters of the retrieved
        // web page content.
        String response = "";
        OutputStream os;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set up connection
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000/*milliseconds*/);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Initialize the transmission data
            String message=" ";

            switch (command) {
                case "set_dog_info":
                    // link the dog info to sensor data in server
                    message = "{ \"age\":" + DogAge_int + ", \"breed\":\"" + DogBreed + "\", \"gender\":\""+ DogGender +"\" }";
                    break;
                default:
                    // connection test
                    message = "{'type': 0}";
                    break;
            }


            // transmit the data to server
            os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(message);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            Log.d("D", "" + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String line = br.readLine();
                while (line != null) {
                    response += line;
                    line = br.readLine();
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
