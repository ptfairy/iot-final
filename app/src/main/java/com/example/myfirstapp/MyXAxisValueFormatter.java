package com.example.myfirstapp;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by Fuwy on 12/13/16.
 */

public class MyXAxisValueFormatter implements IAxisValueFormatter {

    private DecimalFormat mFormat;

    public MyXAxisValueFormatter() {

        // format values to 1 decimal digit
        mFormat = new DecimalFormat("###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        // "value" represents the position of the label on the axis (x or y)
        return mFormat.format(value) + ".h";
    }

    /** this is only needed if numbers are returned, else return 0 */
    public int getDecimalDigits() { return 1; }


}
