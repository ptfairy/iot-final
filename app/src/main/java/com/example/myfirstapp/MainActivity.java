package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private String CollarAddress;
    private String ServerAddress;
    private String DogName;
    private String DogBreed;
    private String DogGender;
    private String DogAge;
    private String DogPulse;
    private String DogTemp;
    private double DogDist;

    private BarChart barChart;

    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barChart = (BarChart) findViewById(R.id.distance_chart);
        DogDist = 0.;

        ((EditText) findViewById(R.id.edit_message)).setText("Doge");

        ServerAddress = "http://ec2-54-213-56-34.us-west-2.compute.amazonaws.com";
        CollarAddress = "http://127.0.0.1:8080";

        new GetDataFromServer().execute(ServerAddress, "distance");
        new GetDataFromServer().execute(ServerAddress, "live_data");

        Button bt = (Button) findViewById(R.id.button_refresh);
        sendMessage(bt);
    }

    @Override
    protected void onResume() {
        super.onResume();

        new GetDataFromServer().execute(ServerAddress, "distance");
        new GetDataFromServer().execute(ServerAddress, "live_data");

        ((TextView) findViewById(R.id.dog_name)).setText(DogName);
        ((TextView) findViewById(R.id.dog_age)).setText(DogAge);
        ((TextView) findViewById(R.id.dog_breed)).setText(DogBreed);
        ((TextView) findViewById(R.id.dog_gender)).setText(DogGender);
        ((TextView) findViewById(R.id.dog_pulse)).setText(DogPulse);
        ((TextView) findViewById(R.id.dog_temp)).setText(DogTemp);
        ((TextView) findViewById(R.id.dog_dist)).setText(Double.toString(DogDist));

        barChart.invalidate();
    }


    /**
     * Called when the user clicks the Send button
     */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        DogName = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, DogName);
        startActivity(intent);
        // Do something in response to button

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        DogAge = preferences.getString("DogAge", "");
        DogBreed = preferences.getString("DogBreed", "");
        DogGender = preferences.getString("DogGender", "");
        ServerAddress = preferences.getString("ServerAddress", "");
        CollarAddress = preferences.getString("CollarAddress", "");

        editor.putString("DogAge", DogAge);
        editor.putString("DogBreed", DogBreed);
        editor.putString("DogGender", DogGender);
        editor.putString("ServerAddress", ServerAddress);
        editor.putString("CollarAddress", CollarAddress);
        editor.apply();

    }

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class GetDataFromServer extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... args) {
            // parameters comes from the execute() call:
            // params[0] is the url
            // params[1] is the commend type
            try {
                String url = args[0];
                String command = args[1];
                String return_text = GetDataRequest(url, command);

                if (return_text == "") {
                    return "Nothing received";
                }
                //  Do Correspondent update
                switch (command) {
                    case "live_data":
                        // retrieve current health data and emotion
                        UpdateLiveStatus(return_text);
                        break;
                    case "distance":
                        // retrieve data according to hour
                        UpdateDistanceChart(return_text);
                        break;
                    default:
                        // connection test
                        break;
                }

                return "Success Update";

            } catch (Exception e) {
                return "Unable to retrieve web page. Url may be invalid: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("CONN", result);
            // get the input data
        }
    }

    // send the post request to the target URL
    @Nullable
    private String GetDataRequest(String my_url, String command) throws IOException {

        // Only display the first 500 characters of the retrieved
        // web page content.
        String response = "";
        String url_new = "";
        switch (command) {
            case "live_data":
                // retrieve current health data and emotion
                url_new = my_url + "/get_live_data/";
                break;
            case "distance":
                // retrieve data according to hour
                url_new = my_url + "/distance/";
                break;
            default:
                // connection test
                break;
        }


        try {
            URL url = new URL(url_new);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set up connection
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000/*milliseconds*/);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            // Initialize the transmission data
            conn.connect();

            int responseCode = conn.getResponseCode();
            Log.d("D", "" + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                String line = br.readLine();
                while (line != null) {
                    response += line;
                    line = br.readLine();
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public void UpdateLiveStatus(String live_data) {
        try {
            JSONObject result = new JSONObject(live_data);
            double pulse = result.getDouble("pulse");
            double temp = result.getDouble("temp");

            DogPulse = Double.toString(pulse);
            DogTemp = Double.toString(temp);
//            double time = result.getDouble("t");
            Log.d("update", "live status");

        } catch (Exception e) {
            Log.d("D", "Error live data");
            e.printStackTrace();
        }

    }

    public void UpdateDistanceChart(String chart_data) {
        try {
            JSONObject jsonObject = new JSONObject(chart_data);
            JSONArray dataArray = jsonObject.getJSONArray("result");
            JSONObject data;

            Log.d("len", " " + dataArray.length());
            DogDist = 0;
            List<BarEntry> bar_entries = new ArrayList<>();
            for (int i = 0; i < dataArray.length(); i++) {
                data = dataArray.getJSONObject(i);
                double distance = data.getDouble("distance");
                double time = data.getDouble("t_begin");

                DogDist += distance;
                int hour = (int) (time % (60 * 60 * 24)) / (60 * 60);
                bar_entries.add(new BarEntry(hour, (float) distance));
            }

            BarDataSet dataSet = new BarDataSet(bar_entries, "Distance");
            BarData lineData = new BarData(dataSet);
            Description desc = new Description();
            XAxis xaxis = barChart.getXAxis();
            xaxis.setValueFormatter(new MyXAxisValueFormatter());
            desc.setText("Hourly activity");
            desc.setTextSize(14);
            barChart.setDescription(desc);
            barChart.setData(lineData);
            barChart.setVisibleXRangeMinimum(3f);


            Log.d("update", "live status");
//            lineChart.invalidate();

        } catch (Exception e) {
            Log.d("D", "Error chart data");
            e.printStackTrace();
        }
    }

    public void RefreshPage(View view) {
        new GetDataFromServer().execute(ServerAddress, "distance");
        new GetDataFromServer().execute(ServerAddress, "live_data");

        ((TextView) findViewById(R.id.dog_name)).setText(DogName);
        ((TextView) findViewById(R.id.dog_age)).setText(DogAge);
        ((TextView) findViewById(R.id.dog_breed)).setText(DogBreed);
        ((TextView) findViewById(R.id.dog_gender)).setText(DogGender);
        ((TextView) findViewById(R.id.dog_pulse)).setText(DogPulse);
        ((TextView) findViewById(R.id.dog_temp)).setText(DogTemp);
        ((TextView) findViewById(R.id.dog_dist)).setText(Double.toString(DogDist));

        barChart.invalidate();
    }
}