# This part combines the IR sensor reading and Pulse sensor reading,
# as well as sending them to the server

import machine
from machine import ADC, Pin, I2C, PWM
from neopixel import NeoPixel
import utime

# IR address
# ir_tobj1 = const(0x07)
# ir_tobj2 = const(0x08)
#
# # EEPROM
# ir_tomax = const(0x20)
# ir_tomin = const(0x21)
# ir_pwmctrl = const(0x22)
# ir_tarange = const(0x23)
# ir_emiss = const(0x24)
# ir_config = const(0x25)
# ir_addr = const(0x0e)
# pulse sensor pin
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=38400)
# accelerometer pin
cs = Pin(16, Pin.OUT)
sdo = Pin(12, Pin.OUT)
sda = Pin(13,  Pin.OUT)
scl = Pin(14,  Pin.OUT)
# infrared sensor pin
adc = ADC(0)
# a = Pin(15, Pin.OUT)
# NeoPixel pin
neo_p = NeoPixel(Pin(15, Pin.OUT), 30)
emotion = 0
'''
emotion mapping:
1 =        //blue
2 = negative    //blue + green
3 = neutral     //green
4 = positive    //red + green
5 = active      //red
'''


def connect():
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect('Columbia University', '')
        # wlan.connect('iPhone-wf', 'qazwsxedc')
        # wlan.connect('TC8715D3F', 'TC8715D4DAF3F')

        while not wlan.isconnected():
            pass
    # ip address of esp8266
    # print("success, host ip is: ", wlan.ifconfig()[0])
    return wlan.ifconfig()[0]


def read_temp():
    read_tobj1 = bytearray(2)
    i2c.readfrom_mem_into(0x5a, 0x07, read_tobj1)
    tobj1_C = (read_tobj1[0] + 256 * read_tobj1[1]) * 0.02 - 273.15
    return tobj1_C


def read_axis():
    import ustruct
    import gc
    from machine import SPI

    spi = SPI(1, baudrate=800000, polarity=1, phase=1)

    cs.value(0)
    spi.write(b'\x2d')  # address of POWER_CTRL
    spi.write(b'\x08')  # start measurement mode
    cs.value(1)

    cs.value(0)
    spi.write(b'\x31')  # address of DATA_FORMAT
    spi.write(b'\x00')  # 4-wire mode
    cs.value(1)

    x1 = bytearray(1)
    x2 = bytearray(1)
    y1 = bytearray(1)
    y2 = bytearray(1)
    z1 = bytearray(1)
    z2 = bytearray(1)

    cs.value(0)
    spi.write(b'\xf2')
    spi.readinto(x1)
    spi.readinto(x2)
    spi.readinto(y1)
    spi.readinto(y2)
    spi.readinto(z1)
    spi.readinto(z2)
    cs.value(1)

    x = int((ustruct.unpack('b', x2)[0] << 8) | ustruct.unpack('b', x1)[0])
    y = int((ustruct.unpack('b', y2)[0] << 8) | ustruct.unpack('b', y1)[0])
    z = int((ustruct.unpack('b', z2)[0] << 8) | ustruct.unpack('b', z1)[0])
    gc.collect()
    return [x, y, z]


def bmp_calculate():
    sample_counter = 0          # keep track of current time(ms)
    # n = 0                       # keep track of time interval to avoid noise
    last_beat_time = 0          # keep track of the time of last beat(ms)
    thresh = 550                # 512 * 3.3 * (220/(220+460)) = 550 which is the mean value if no pulse detected
    ibi = 600                   # time interval between beats(ms)
    pulse = False               #
    # current_state = False       # become true when a beat is found
    first_beat = True           #
    second_beat = False         #
    p = 550                     # peak of a pulse
    t = 550                     # dichroic notch, i.e. trough of a pulse
    rate = [0] * 10             # holds ten IBI values
    # amp = 100                   # holds the amplitude of pulse waveform
    # flag = False

    current_time = utime.ticks_ms()

    # main loop
    while True:
        signal = adc.read()  # signal read from ADC
        sample_counter += utime.ticks_ms() - current_time
        current_time = utime.ticks_ms()        # keep track of current time in ms
        n = sample_counter - last_beat_time    # keep track of time interval to avoid noise

        if signal < thresh and n > (ibi * 0.6):
            if signal < t:
                t = signal      # keep track of the trough

        if signal > thresh and signal > p:
            p = signal          # keep track of the peak

        if n > 250:             # filter those high frequency noise
            # according to the pulse detection definition
            if signal > thresh and pulse is False and n > (ibi * 0.6):
                pulse = True
                # a.value(1)
                # a.value(0)
                ibi = sample_counter - last_beat_time
                last_beat_time = sample_counter

                if second_beat:
                    second_beat = False
                    for i in range(0, 10, 1):
                        rate[i] = ibi

                if first_beat:
                    first_beat = False
                    second_beat = True
                    continue

                running_total = 0

                for i in range(0, 9, 1):
                    rate[i] = rate[i+1]
                    running_total += rate[i]

                rate[9] = ibi
                running_total += rate[9]
                running_total /= 10
                bmp = 60000 / running_total
                # bmp_last = bmp
                if 60 < bmp < 210:
                    return bmp
                return 0
                #     print("BMP: ", bmp)
                #     return bmp
                # else:
                #     a.value(0)
            if signal < thresh and pulse is True:
                pulse = False
                # a.value(0)
                amp = p - t
                thresh = amp / 2 + t
                p = thresh
                t = thresh

            if n > 2500:
                thresh = 550
                p = 550
                t = 550
                last_beat_time = sample_counter
                first_beat = True
                second_beat = False
                # a.value(0)
                # print("no beats found")
            # utime.sleep_ms(1)
    print("end call")


def send_info(dog, b_info):
    import usocket as socket
    import gc
    global emotion
    axis = read_axis()
    t_obj = read_temp()
    content = '{"X":' + str(axis[0]) + ', "Y":' + str(axis[1]) + ', "Z":' + str(axis[2]) + ', ' + str(dog[0]) \
            + ', ' + dog[1] + ', ' + dog[2] + ', "pulse":' + str(b_info) \
            + ', "temp":' + str(t_obj) + '}'
    # print("content: ", content)
    a_request = str('POST /train_collar_data/ HTTP/1.0\r\nHost: ec2-54-213-56-34.us-west-2.compute.amazonaws.com\r\n')
    # a_request = str('POST /test_emotion/ HTTP/1.0\r\nHost: ec2-54-213-56-34.us-west-2.compute.amazonaws.com\r\n')
    a_request += "Connection: close\r\n"
    a_request += "Content-Type: application/json\r\n"
    a_request += 'Content-Length: ' + str(len(content)) + '\r\n\r\n'
    a_request += content
    # s.connect(addr)
    s = socket.socket()  # default as TCP
    try:
        s.connect(socket.getaddrinfo('ec2-54-213-56-34.us-west-2.compute.amazonaws.com', 80)[0][-1])
        s.send(bytes(a_request, 'utf-8'))
        print("content: ", content)
        emotion = str(s.read())
        emotion = emotion.split('{')[-1]
        emotion = emotion.split('}')[0]
        emotion = int(emotion.split(':')[-1])
        s.close()
        print("emotion: ", emotion)
        gc.collect()
    except OSError:
        print("server is down")


def receive_info(local_ip):
    import usocket as socket
    import gc
    # import json
    # server configuration
    s = socket.socket()
    addr = socket.getaddrinfo(local_ip, 8080)[0][-1]
    print('Connected! Listening to ' + local_ip)

    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(addr)
    s.listen(1)
    print("waiting for dog's information")
    feedback = b"""\
HTTP/1.1 200 OK
Content-Length: 28
Content-Type: text/html
Connection: close

Message Received by ESP8266!
    """
    data = None
    # s.settimeout(1)
    client_sock = s.accept()[0]
    client_sock.setblocking(False)
    # data = None
    while data is None:
        data = client_sock.read()
    client_sock.send(feedback)
    client_sock.close()
    s.close()
    gc.collect()

    data = str(data)
    data = data.split('{')[1]
    data = data.split('}')[0]
    info = data.split(',')
    print("info received: ", info)
    return info


def neo_control():
    # from neopixel import NeoPixel
    # import utime
    import gc
    mode = [(1, 1, 1), (0, 0, 1), (0, 1, 1), (0, 1, 0), (1, 1, 0), (1, 0, 0)]
    # fade in/out
    for i in range(0, 2 * 256, 16):
        for j in range(30):
            if (i // 256) % 2 == 0:
                val = i & 0xff
            else:
                val = 255 - (i & 0xff)
            # print("mode: ", mode[emotion])
            # neo_p[j] = mode[emotion] * val
            neo_p[j] = (mode[emotion][0] * val, mode[emotion][1] * val, mode[emotion][2] * val)
            # neo_p[j] = (val, 0, 0)
        neo_p.write()
        # utime.sleep_ms(25)
        # utime.sleep_ms(50)
    # clear
    for i in range(30):
        neo_p[i] = (0, 0, 0)
    neo_p.write()
    # utime.sleep_ms(50)
    gc.collect()

# default_info = ['"age": 0', '"breed": "Default"', '"gender": "Male"']
# connect()

ip_addr = connect()
dog_info = receive_info(ip_addr)
while True:
    neo_control()
    if dog_info is not None:
        bmp_info = bmp_calculate()
        if bmp_info > 0:
            send_info(dog_info, bmp_info)
            # neo_control()

#