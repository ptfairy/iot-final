import time
import numpy as np

from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request
from pymongo import MongoClient
from scipy.signal import find_peaks_cwt

from test import test_mlp

app = Flask(__name__)


@app.route('/')
def hello():
    return 'hello'


client = MongoClient()
db = client.restdb
# result = db.acc.remove({})

app.config['MONGO_DBNAME'] = 'restdb'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/restdb'


@app.route('/POST', methods=['POST'])
def get_acc():
    x = request.json['X']
    y = request.json['Y']
    z = request.json['Z']
    ISOTIMEFORMAT = '%Y-%m-%d %X'
    timetime = time.strftime(ISOTIMEFORMAT, time.localtime())
    db_id = db.acc.insert({'X': x, 'Y': y, 'Z': z, 't': timetime})
    new_acc = db.acc.find_one({'_id': db_id})
    output = {'X': new_acc['X'], 'Y': new_acc['Y'], 'Z': new_acc['Z'], 't': new_acc['t']}
    return jsonify({'result': output})


@app.route('/GET/', methods=['GET'])
def get_all_acc():
    output = []
    for s in db.acc.find({'type': 'info'}):
        output.append({'X': s['X'], 'Y': s['Y'], 'Z': s['Z'], 't': s['t']})
    return jsonify({'result': output})


@app.route('/GET2/', methods=['GET'])
def get_last_acc():
    output = []
    for s in db.acc.find().sort('_id', -1).limit(1):
        output.append({'X': s['X'], 'Y': s['Y'], 'Z': s['Z'], 't': s['t']})
    return jsonify({'result': output})


@app.route('/show/')
@app.route('/show/<flag>')
def show(flag=None):
    return render_template('live_update_script.html', flag=flag)


######################
# deep learning part #
######################

# get the post data from client
@app.route('/train_collar_data/', methods=['POST'])
def store_data():
    emotion = 4
    # get the data from post requests
    x = request.json['X']
    y = request.json['Y']
    z = request.json['Z']
    age = request.json['age']
    breed = request.json['breed']
    gender = request.json['gender']
    pulse = request.json['pulse']
    temp = request.json['temp']

    # data selection
    if pulse < 60 or pulse > 200:
        emotion = 0
        return "{'emotion':" + str(emotion) + "}"

    t = time.time() + 60*58
    db.acc.insert({
        'type': 'info',
        'emotion': emotion,
        'X': x,
        'Y': y,
        'Z': z,
        'age': age,
        'breed': breed,
        'gender': gender,
        'pulse': pulse,
        'temp': temp,
        't': t})

    return "{'emotion':" + str(emotion) + "}"


@app.route('/get_live_data/', methods=['GET'])
def get_live_data():
    json_obj = jsonify({
        'type': 'test',
        'X': 0,
        'Y': 0,
        'Z': 0,
        'age': 0,
        'breed': 'N/A',
        'gender': 'N/A',
        'pulse': 0,
        'temp': 0,
        't': 0})
    for s in db.acc.find({'$or': [{'type': 'info'}, {'type': 'test'}]}).sort('_id', -1).limit(1):
        # for s in db.acc.find({'type': 'info'}).sort('_id', -1):
        json_obj = jsonify({
            'type': s['type'],
            'X': s['X'],
            'Y': s['Y'],
            'Z': s['Z'],
            'age': s['age'],
            'breed': s['breed'],
            'gender': s['gender'],
            'pulse': s['pulse'],
            'temp': s['temp'],
            't': s['t']})
    return json_obj


# parse the data from data base into proper format for ANN input
@app.route('/parse/', methods=['GET'])
def parse_data():
    # now_time = time.time()
    data_set = []
    breed_dic = dict()
    emotion_dic = dict()
    breed_i = 0
    emotion_i = 0
    first = True

    last_x = 0
    last_y = 0
    last_z = 0
    for s in db.acc.find({'type': 'info'}).sort('_id', 1):
        if first:
            last_x = s['X']
            last_y = s['Y']
            last_z = s['Z']

        x = s['X']
        y = s['Y']
        z = s['Z']

        dx = x - last_x
        dy = y - last_y
        dz = z - last_z

        last_x = x
        last_y = y
        last_z = z

        age = int(s['age'])

        if s['gender'] == 'Male':
            gender = 0
        else:
            gender = 1
        pulse = s['pulse']
        temp = s['temp']

        breed = s['breed']
        if not breed_dic.__contains__(breed):
            breed_i += 1
            breed_dic[breed] = breed_i
            breed = breed_i
        else:
            breed = breed_dic[breed]

        emotion = s['emotion']
        if not emotion_dic.__contains__(emotion):
            emotion_i += 1
            emotion_dic[emotion] = emotion_i
            emotion = emotion_i
        else:
            emotion = emotion_dic[emotion]

        data = list([dx, dy, dz, age, breed, gender, pulse, temp, emotion])
        data_set.append(data)

    data_set = np.asarray(data_set)
    return data_set, breed_dic, emotion_dic


# get the steps info
@app.route('/steps/', methods=['GET'])
def get_activity_steps():
    now_time = time.time()
    time_span = 2  # min data width counting for a step

    # the beginning time of the day
    today_time = now_time - now_time % (24 * 60 * 60)

    # all the steps that has been calculated today
    step_all = db.acc.find({'type': 'steps', 't': {'$gt': today_time}}).sort('_id', -1)

    # gather all steps in results
    last_step_time = today_time
    result = []
    if step_all is not None:
        for s in step_all:
            result.append({'steps': s['steps'], 't_begin': s['t_begin'], 't': s['t']})
            # get the last step time in data_set
            last_step_time = s['t']

    # steps needs to calculate
    data_set = db.acc.find({'type': 'info', 't': {'$gt': last_step_time}}).sort('_id', -1)

    data_x = []
    data_y = []
    data_z = []

    for s in data_set:  # extract data
        if s['t'] >= last_step_time + 60 * 60:
            # calculate the hour data

            data_acc = np.asarray(data_x) ** 2 + np.asarray(data_y) ** 2 + np.asarray(data_z) ** 2

            if len(data_acc) <= 1:
                steps = 0
            else:
                peaks = find_peaks_cwt(data_acc, np.arange(1, time_span))
                steps = len(peaks)

            begin_time = s['t'] - s['t'] % (60 * 60)

            result.append({
                'type': 'steps',
                't_begin': begin_time,
                't': last_step_time + 60 * 60,
                'steps': steps})
            db.acc.insert({
                'type': 'steps',
                't_begin': begin_time,
                't': last_step_time + 60 * 60,
                'steps': steps})

            # clear the data set and prepare for the next hour
            data_x = []
            data_y = []
            data_z = []
            last_step_time += 60 * 60

        # append the new data into data set
        data_x.append(s['X'])
        data_y.append(s['Y'])
        data_z.append(s['Z'])

    # calculate the steps within the last hour
    data_acc = np.asarray(data_x) ** 2 + np.asarray(data_y) ** 2 + np.asarray(data_z) ** 2

    if len(data_acc) <= 1:
        steps = 0
    else:
        peaks = find_peaks_cwt(data_acc, np.arange(1, time_span))
        steps = len(peaks)

    result.append({
        'type': 'steps',
        't_begin': last_step_time,
        't': now_time,
        'steps': steps})

    print(len(data_acc))
    return jsonify({'result': result})


# get the steps info
# @app.route('/distance/', methods=['GET'])
@app.route('/distance/')
def get_activity_distance():
    now_time = time.time()

    # the beginning time of the day
    today_time = now_time - now_time % (24 * 60 * 60)

    # all the distance that has been calculated today
    distance_all = db.acc.find({'type': 'distance', 't': {'$gt': today_time}}).sort('_id', 1)

    # gather all distance in results
    last_distance_time = today_time
    result = []
    if distance_all is not None:
        for s in distance_all:
            result.append({'distance': s['distance'], 't_begin': s['t_begin'], 't': s['t']})
            # get the last step time in data_set
            last_distance_time = s['t']

    # distance needs to calculate
    data_set = db.acc.find({'$or': [{'type': 'info'}, {'type': 'test'}], 't': {'$gt': last_distance_time}}).sort('_id',
                                                                                                                 1)

    distance = 0
    last_x = 0
    last_y = 0
    last_z = 0
    last_t = last_distance_time
    i = 0

    for s in data_set:  # extract data
        if i == 0:
            i += 1
            last_t = s['t']
            last_distance_time = s['t'] - s['t'] % (60 * 60)
            last_x = s['X'] * 9.8 / 1000
            last_y = s['Y'] * 9.8 / 1000
            last_z = s['Z'] * 9.8 / 1000
        else:
            if s['t'] >= last_distance_time + 60 * 60:
                # calculate the hour data
                result.append({
                    'type': 'distance',
                    't_begin': last_distance_time,
                    't': last_distance_time + 60 * 60,
                    'distance': distance})
                db.acc.insert({
                    'type': 'distance',
                    't_begin': last_distance_time,
                    't': last_distance_time + 60 * 60,
                    'distance': distance})

                # clear the data set and prepare for the next hour
                distance = 0
                last_distance_time += 60 * 60
                print(s['t'])
                print(last_distance_time)

            # store the data
            x = s['X'] * 9.8 / 1000
            y = s['Y'] * 9.8 / 1000
            z = s['Z'] * 9.8 / 1000
            t = s['t']
            data_acc = (x - last_x) ** 2 + (y - last_y) ** 2 + (z - last_z) ** 2

            if abs(t - last_t) <= 4:
                distance += np.sqrt(data_acc) * (t - last_t) ** 2 / 2

            last_x = x
            last_y = y
            last_z = z
            last_t = t

    # calculate the distance within the last hour

    result.append({
        'type': 'distance',
        't_begin': last_distance_time,
        't': now_time,
        'distance': distance})

    return jsonify({'result': result})


# use for debug
@app.route('/clear_step')
def clear_steps():
    db.acc.remove({'type': 'steps'})
    db.acc.remove({'type': 'distance'})
    return "clear steps & distance"


# use for debug
@app.route('/clear_all')
def clear_all():
    db.acc.remove()
    return "clear all"


# use for debug
@app.route('/show_emotion')
def show_emotion():
    emo_1 = db.acc.find({'emotion': 1, 'type': 'info'}).count()
    emo_2 = db.acc.find({'emotion': 2, 'type': 'info'}).count()
    emo_3 = db.acc.find({'emotion': 3, 'type': 'info'}).count()
    emo_4 = db.acc.find({'emotion': 4, 'type': 'info'}).count()
    emo_5 = db.acc.find({'emotion': 5, 'type': 'info'}).count()
    # output.append({'steps': s['steps'], 't_begin': s['t_begin'], 't': s['t']})
    return str(emo_1) + ',' + str(emo_2) + ',' + str(emo_3) + ',' + str(emo_4) + ',' + str(emo_5)


@app.route('/test_emotion/', methods=['POST'])
def test_emotion():
    _, breed_dic, emotion_dic = parse_data()

    x = request.json['X']
    y = request.json['Y']
    z = request.json['Z']
    age = int(request.json['age'])

    gender = request.json['gender']
    pulse = request.json['pulse']
    temp = request.json['temp']

    # data selection
    if pulse < 60 or pulse > 200:
        emotion = 0
        return "{'emotion':" + str(emotion) + "}"

    breed = request.json['breed']

    # new breed marks as 'other'
    if not breed_dic.__contains__(breed):
        breed = breed_dic.__len__() + 1
    else:
        breed = breed_dic[breed]

    if gender == 'Male':
        gender = 0
    else:
        gender = 1

    # get the last activity
    last_x = x
    last_y = y
    last_z = z

    for s in db.acc.find({
        '$or': [{'type': 'info'}, {'type': 'test'}],
        'age': request.json['age'],
        'gender': request.json['gender'],
        'breed': request.json['breed']
    }).sort('_id', -1).limit(1):
        last_x = s['X']
        last_y = s['Y']
        last_z = s['Z']

    dx = x - last_x
    dy = y - last_y
    dz = z - last_z

    data = np.asarray(list([dx, dy, dz, age, breed, gender, pulse, temp])).reshape(1, -1)
    emotion = test_mlp(data)[0]
    idx2word = dict((k, v) for v, k in emotion_dic.items())
    emotion = idx2word[emotion]

    t = time.time()
    db.acc.insert({
        'type': 'test',
        'emotion': emotion,
        'X': request.json['X'],
        'Y': request.json['Y'],
        'Z': request.json['Z'],
        'age': request.json['age'],
        'breed': request.json['breed'],
        'gender': request.json['gender'],
        'pulse': request.json['pulse'],
        'temp': request.json['temp'],
        't': t})

    return "{'emotion':" + str(emotion) + "}"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)


    # ref: https://www.mathworks.com/help/supportpkg/mobilesensor/examples/counting-steps-by-capturing-acceleration-data-from-your-android-device.html
    # ref: https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.signal.find_peaks_cwt.html
