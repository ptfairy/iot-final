"""
This code is based on
[1] http://deeplearning.net/tutorial/logreg.html
[2] http://deeplearning.net/tutorial/mlp.html
"""

from __future__ import print_function

__docformat__ = 'restructedtext en'

import os
import sys
import timeit

import numpy
import scipy.io

import theano
import theano.tensor as T

from pymongo import MongoClient


def parse_data():
    client = MongoClient()
    db = client.restdb

    data_set = []
    breed_dic = dict()
    emotion_dic = dict()
    breed_i = 0
    emotion_i = 0

    for s in db.acc.find({'type': 'info'}):
        X = s['X']
        Y = s['Y']
        Z = s['Z']
        age = int(s['age'])

        if s['gender'] == 'Male':
            gender = 0
        else:
            gender = 1
        pulse = s['pulse']
        temp = s['temp']

        breed = s['breed']
        if not breed_dic.__contains__(breed):
            breed_i += 1
            breed_dic[breed] = breed_i
            breed = breed_i
        else:
            breed = breed_dic[breed]

        emotion = s['emotion']
        if not emotion_dic.__contains__(emotion):
            emotion_i += 1
            emotion_dic[emotion] = emotion_i
            emotion = emotion_i
        else:
            emotion = emotion_dic[emotion]

        data = list([X, Y, Z, age, breed, gender, pulse, temp, emotion])
        data_set.append(data)

    data_set = numpy.asarray(data_set)
    return data_set, breed_dic, emotion_dic


def load_data():
    ''' Loads the street view house numbers (SVHN) dataset

    This function is modified from load_data in
    http://deeplearning.net/tutorial/code/logistic_sgd.py
    '''

    #############
    # LOAD DATA #
    #############

    # Download the SVHN dataset if it is not present
    def check_dataset(dataset):
        # Check if dataset is in the data directory.
        new_path = os.path.join(
            os.path.split(__file__)[0],
            "..",
            "data",
            dataset
        )
        if (not os.path.isfile(new_path)):
            from six.moves import urllib
            origin = (
                'http://ufldl.stanford.edu/housenumbers/' + dataset
            )
            print('Downloading data from %s' % origin)
            urllib.request.urlretrieve(origin, new_path)
        return new_path

    train_dataset = check_dataset('train_32x32.mat')
    test_dataset = check_dataset('test_32x32.mat')

    print('... loading data')

    # Load the dataset
    train_set = scipy.io.loadmat(train_dataset)
    test_set = scipy.io.loadmat(test_dataset)

    # Convert data format for usage in Theano
    def convert_data_format(data):
        X = numpy.reshape(data['X'],
                          (numpy.prod(data['X'].shape[:-1]), data['X'].shape[-1]),
                          order='C').T / 255.
        y = data['y'].flatten()
        y[y == 10] = 0  # '0' has label 10 in the SVHN dataset
        return (X, y)

    train_set = convert_data_format(train_set)
    test_set = convert_data_format(test_set)

    # Extract validation dataset from train dataset
    train_set_len = len(train_set[1])
    valid_set = (x[-(train_set_len // 10):] for x in train_set)
    train_set = (x[:-(train_set_len // 10)] for x in train_set)

    # train_set, valid_set, test_set format: tuple(input, target)
    # input is a numpy.ndarray of 2 dimensions (a matrix)
    # where each row corresponds to an example. target is a
    # numpy.ndarray of 1 dimension (vector) that has the same length as
    # the number of rows in the input. It should give the target
    # to the example with the same index in the input.
    def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(numpy.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(numpy.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval


class LogisticRegression(object):
    """Multi-class Logistic Regression Class

    The logistic regression is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`. Classification is done by projecting data
    points onto a set of hyperplanes, the distance to which is used to
    determine a class membership probability.
    """

    def __init__(self, input, n_in, n_out):
        """ Initialize the parameters of the logistic regression

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
                      architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
                     which the datapoints lie

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
                      which the labels lie

        """
        # initialize with 0 the weights W as a matrix of shape (n_in, n_out)
        self.W = theano.shared(
            value=numpy.zeros(
                (n_in, n_out),
                dtype=theano.config.floatX
            ),
            name='W',
            borrow=True
        )
        # initialize the biases b as a vector of n_out 0s
        self.b = theano.shared(
            value=numpy.zeros(
                (n_out,),
                dtype=theano.config.floatX
            ),
            name='b',
            borrow=True
        )

        # symbolic expression for computing the matrix of class-membership
        # probabilities
        # Where:
        # W is a matrix where column-k represent the separation hyperplane for
        # class-k
        # x is a matrix where row-j  represents input training sample-j
        # b is a vector where element-k represent the free parameter of
        # hyperplane-k
        self.p_y_given_x = T.nnet.softmax(T.dot(input, self.W) + self.b)

        # symbolic description of how to compute prediction as class whose
        # probability is maximal
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)

        # parameters of the model
        self.params = [self.W, self.b]

        # keep track of model input
        self.input = input

    def negative_log_likelihood(self, y):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|}
                \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
            \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.neq(self.y_pred, y))
        else:
            raise NotImplementedError()


def drop(input, p=0.5):
    """
    :type input: numpy.array
    :param input: layer or weight matrix on which dropout is applied

    :type p: float or double between 0. and 1.
    :param p: p probability of NOT dropping out a unit, therefore (1.-p) is the drop rate.

    """
    rng = numpy.random.RandomState(1234)
    srng = T.shared_randomstreams.RandomStreams(rng.randint(999999))
    mask = srng.binomial(n=1, p=p, size=input.shape, dtype=theano.config.floatX)
    return input * mask


class HiddenLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
        """
        self.input = input

        # `W` is initialized with `W_values` which is uniformely sampled
        # from sqrt(-6./(n_in+n_hidden)) and sqrt(6./(n_in+n_hidden))
        # for tanh activation function
        # the output of uniform if converted using asarray to dtype
        # theano.config.floatX so that the code is runable on GPU
        # Note : optimal initialization of weights is dependent on the
        #        activation function used (among other things).
        #        For example, results presented in [Xavier10] suggest that you
        #        should use 4 times larger initial weights for sigmoid
        #        compared to tanh
        #        We have no info for other function, so we use the same as
        #        tanh.
        if W is None:
            W_values = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b

        lin_output = T.dot(input, self.W) + self.b
        self.output = (
            lin_output if activation is None
            else activation(lin_output)
        )
        # parameters of the model
        self.params = [self.W, self.b]


class DropoutHiddenLayer(object):
    def __init__(self, rng, is_train, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh, p=0.5):
        """
        Hidden unit activation is given by: activation(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type is_train: theano.iscalar
        :param is_train: indicator pseudo-boolean (int) for switching between training and prediction

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer

        :type p: float or double
        :param p: probability of NOT dropping out a unit
        """
        self.input = input

        if W is None:
            W_values = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b

        lin_output = T.dot(input, self.W) + self.b

        output = activation(lin_output)

        # multiply output and drop -> in an approximation the scaling effects cancel out
        train_output = drop(output, p)

        # is_train is a pseudo boolean theano variable for switching between training and prediction
        self.output = T.switch(T.neq(is_train, 0), train_output, p * output)

        # parameters of the model
        self.params = [self.W, self.b]


class myMLP(object):
    def __init__(self, rng, input, n_in, n_hidden, n_out, training_enabled, drop_p=0.8, n_hiddenLayers=2):

        self.hiddenLayers = []
        for i in range(n_hiddenLayers):
            h_input = input if i == 0 else self.hiddenLayers[i - 1].output
            h_in = n_in if i == 0 else n_hidden
            self.hiddenLayers.append(
                DropoutHiddenLayer(
                    rng=rng,
                    is_train=training_enabled,
                    input=h_input,
                    n_in=h_in,
                    n_out=n_hidden,
                    p=drop_p,
                    activation=T.tanh,
                ))

        self.logRegressionLayer = LogisticRegression(
            input=self.hiddenLayers[-1].output,
            n_in=n_hidden,
            n_out=n_out
        )

        # L1 norm ; one regularization option is to enforce L1 norm to
        # be small
        self.L1 = (
            sum([abs(x.W).sum() for x in self.hiddenLayers])
            + abs(self.logRegressionLayer.W).sum()
        )

        # square of L2 norm ; one regularization option is to enforce
        # square of L2 norm to be small
        self.L2_sqr = (
            sum([(x.W ** 2).sum() for x in self.hiddenLayers])
            + (self.logRegressionLayer.W ** 2).sum()
        )
        # negative log likelihood of the MLP is given by the negative
        # log likelihood of the output of the model, computed in the
        # logistic regression layer
        self.negative_log_likelihood = self.logRegressionLayer.negative_log_likelihood

        # same holds for the function computing the number of errors
        self.errors = self.logRegressionLayer.errors

        # the parameters of the model are the parameters of the two layer it is
        # made out of
        self.params = sum([x.params for x in self.hiddenLayers], []) + self.logRegressionLayer.params

        # keep track of model input
        self.input = input

        # prediction of the model
        self.output = self.logRegressionLayer.y_pred

    def save(self, folder):
        i = 0
        for param in self.params:
            i += 1
            numpy.save(os.path.join(folder,
                                    param.name + str(i) + '.npy'), param.get_value())

    def load(self, folder):
        i = 0
        for param in self.params:
            i += 1
            param.set_value(numpy.load(os.path.join(folder,
                                                    param.name + str(i) + '.npy')))


def test_mlp_dropout(learning_rate=0.1, L1_reg=0.00, L2_reg=0.0001, n_epochs=10,
                     batch_size=2, n_hidden=50, n_hiddenLayers=2,
                     n_in=32 * 32 * 3, n_out=10, seed=1234,
                     drop_p=0.8, momentum_alpha=0.8,
                     verbose=True, save_model=False, load_model=False):
    """
    Demonstrate stochastic gradient descent optimization for a multilayer
    perceptron

    :type learning_rate: float
    :param learning_rate: learning rate used (factor for the stochastic
    gradient

    :type L1_reg: float
    :param L1_reg: L1-norm's weight when added to the cost (see
    regularization)

    :type L2_reg: float
    :param L2_reg: L2-norm's weight when added to the cost (see
    regularization)

    :type n_epochs: int
    :param n_epochs: maximal number of epochs to run the optimizer

    :type batch_size: int
    :param batch_size: batch_size during each training iteration

    :type n_hidden: int
    :param n_hidden: the number of hidden units in each hidden layer

    :type n_in: int
    :param n_in: dimension of inputs

    :type n_out: int
    :param n_out: dimension of outputs

    :type momentum_alpha: float
    :param momentum_alpha: parameter for momentum update

    :type drop_p: float
    :param drop_p: parameter for drop-out

    :type verbose: boolean
    :param verbose: to print out epoch summary or not to

    :type save_model: boolean
    :param save_model: to save model in folder

    :type load_model: boolean
    :param load_model: to load model in folder

    """

    ####################
    # LOAD THE DATASET #
    ####################

    datasets, _, _ = parse_data()

    train_set_x = theano.shared(numpy.asarray(datasets[:, :-1], dtype=theano.config.floatX), borrow=True)
    train_set_y = theano.shared(numpy.asarray(datasets[:, -1], dtype=theano.config.floatX), borrow=True)
    train_set_y = T.cast(train_set_y, 'int32')

    # compute number of minibatches for training, validation and testing
    n_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size

    # # load the dataset; download the dataset if it is not present
    # datasets = load_data()
    #
    # # datasets = parse_data()
    #
    # train_set_x, train_set_y = datasets[0]
    # valid_set_x, valid_set_y = datasets[1]
    # test_set_x, test_set_y = datasets[2]
    #
    # # compute number of minibatches for training, validation and testing
    # n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    # n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    # n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of # [int] labels

    training_enabled = T.iscalar('training_enabled')  # pseudo boolean for switching between training and prediction

    rng = numpy.random.RandomState(seed)

    mlp = myMLP(
        rng=rng,
        training_enabled=training_enabled,
        input=x,
        n_in=n_in,
        n_hidden=n_hidden,
        n_out=n_out,
        drop_p=drop_p,
        n_hiddenLayers=n_hiddenLayers
    )

    # the cost we minimize during training is the negative log likelihood of
    # the model plus the regularization terms (L1 and L2); cost is expressed
    # here symbolically
    cost = (
        mlp.negative_log_likelihood(y)
        + L1_reg * mlp.L1
        + L2_reg * mlp.L2_sqr
    )

    # compiling a Theano function that computes the mistakes that are made
    # by the model on a minibatch
    validate_model = theano.function(
        inputs=[index],
        outputs=mlp.errors(y),
        givens={
            x: train_set_x[index * batch_size:(index + 1) * batch_size],
            y: train_set_y[index * batch_size:(index + 1) * batch_size],
            training_enabled: numpy.cast['int32'](0)
        }
    )

    # compute the gradient of cost with respect to theta (sotred in params)
    # the resulting gradients will be stored in a list gparams
    gparams = [T.grad(cost, param) for param in mlp.params]

    # specify how to update the parameters of the model as a list of
    # (variable, update expression) pairs
    momentum = theano.shared(numpy.cast[theano.config.floatX](momentum_alpha), name='momentum')

    updates = []
    for param, gparam in zip(mlp.params, gparams):
        param_update = theano.shared(param.get_value() * numpy.cast[theano.config.floatX](0.))
        updates.append((param, param - learning_rate * param_update))
        updates.append((param_update, momentum * param_update +
                        (numpy.cast[theano.config.floatX](1.) - momentum) * gparam))

    # compiling a Theano function `train_model` that returns the cost, but
    # in the same time updates the parameter of the model based on the rules
    # defined in `updates`
    train_model = theano.function(
        inputs=[index],
        outputs=cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size],
            training_enabled: numpy.cast['int32'](1)
        }
    )

    ###############
    # TRAIN MODEL #
    ###############

    folder = 'outputs/'

    # make sure the file is exist
    if not os.path.exists(folder):
        os.makedirs(folder)

    filename = folder + 'IoT learning.txt'
    data_file = open(filename, 'w')

    print('... training')

    # early-stopping parameters
    patience = 100000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
    # found
    improvement_threshold = 0.995  # a relative improvement of this much is
    # considered significant
    validation_frequency = min(n_batches, patience // 2)
    # go through this many
    # minibatche before checking the network
    # on the validation set; in this case we
    # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False

    if load_model:
        mlp.load(folder=folder)

    while (epoch < n_epochs) and (not done_looping):
        epoch += 1
        for minibatch_index in range(n_batches):

            minibatch_avg_cost = train_model(minibatch_index)
            # iteration number
            iter = (epoch - 1) * n_batches + minibatch_index

            if (iter + 1) % validation_frequency == 0:
                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print_line = ('epoch %i, minibatch %i/%i, validation error %f %%' %
                                  (epoch,
                                   minibatch_index + 1,
                                   n_batches,
                                   this_validation_loss * 100.))
                    data_file.write(print_line + '\n')
                    print(print_line)

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:
                    # improve patience if loss improvement is good enough
                    if (
                                this_validation_loss < best_validation_loss *
                                improvement_threshold
                    ):
                        patience = max(patience, iter * patience_increase)

                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    if save_model:
                        mlp.save(folder=folder)

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    #################
    ##saving output##
    #################
    line_1 = (('Optimization complete. Best validation score of %f %% '
               'obtained at iteration %i, with test performance %f %%. ') %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
    line_2 = ('The code for file ran for %.2fm' % ((end_time - start_time) / 60.))

    print(line_1)
    print(('The code for file ' +
           os.path.split(__file__)[1] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)

    data_file.write(line_1 + line_2 + '\n')
    data_file.close()
    print("file saved")

    return mlp


def test_mlp(input_x, n_hidden=50, n_hiddenLayers=2,
             n_in=8, n_out=6, seed=1234, drop_p=0.8):
    x = T.matrix('x')
    training_enabled = T.iscalar('training_enabled')

    rng = numpy.random.RandomState(seed)
    mlp = myMLP(
        rng=rng,
        training_enabled=training_enabled,
        input=x,
        n_in=n_in,
        n_hidden=n_hidden,
        n_out=n_out,
        drop_p=drop_p,
        n_hiddenLayers=n_hiddenLayers
    )

    folder = 'outputs/'
    # make sure the file is exist
    if not os.path.exists(folder):
        os.makedirs(folder)
    mlp.load(folder=folder)



    test_model = theano.function(
        inputs=[x],
        outputs=mlp.output,
        givens={
            training_enabled: numpy.cast['int32'](0)
        }
    )

    result = test_model(input_x)
    return result


if __name__ == '__main__':
    mlp = test_mlp_dropout(n_epochs=10000, n_in=8, n_out=6, batch_size=128, save_model=True, load_model=False)
